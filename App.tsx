import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {Text, View} from 'react-native';
import HomeScreen from './android/src/libraries/component/Home/HomeScreen';
import sizes from './android/src/libraries/sizes/sizes';

const Tab = createMaterialBottomTabNavigator();

function EmptyPlaceholder() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Coming Soon</Text>
    </View>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator style={{height: sizes(48)}} labeled={false}>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Chat" component={EmptyPlaceholder} />
        <Tab.Screen name="Feed" component={EmptyPlaceholder} />
        <Tab.Screen name="Profile" component={EmptyPlaceholder} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
