import React, {useCallback, useState} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {IconButton} from 'react-native-paper';
import sizes from '../../sizes/sizes';

const data = [
  {
    avatar:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    username: 'UsupSuparma',
    date: 'Mar 24, 2022',
    caption:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget nisl a dolor finibus pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce ornare, felis ac placerat suscipit, justo urna placerat lacus, in dapibus nisl dui ac velit. Donec imperdiet laoreet magna vitae suscipit. Vivamus erat elit, fringilla in hendrerit ut, egestas et eros. Nunc posuere posuere arcu, et bibendum libero varius nec. Vivamus fermentum nisi leo, vitae sagittis enim iaculis non. Phasellus viverra cursus nulla.',
    image:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    comment: 37,
    like: 376,
  },

  {
    avatar:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    username: 'UsupSuparma',
    date: 'Mar 24, 2022',
    caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    image:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    comment: 37,
    like: 376,
  },
  {
    avatar:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    username: 'UsupSuparma',
    date: 'Mar 24, 2022',
    caption: null,
    image:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    comment: 37,
    like: 376,
  },
  {
    avatar:
      'https://images.pexels.com/photos/771742/pexels-photo-771742.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    username: 'UsupSuparma',
    date: 'Mar 24, 2022',
    caption:
      ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget nisl a dolor finibus pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce ornare, felis ac placerat suscipit, justo urna placerat lacus, in dapibus nisl dui ac velit. Donec imperdiet laoreet magna vitae suscipit. Vivamus erat elit, fringilla in hendrerit ut, egestas et eros. Nunc posuere posuere arcu, et bibendum libero varius nec. Vivamus fermentum nisi leo, vitae sagittis enim iaculis non. Phasellus viverra cursus nulla. ',
    image: null,
    comment: 37,
    like: 376,
  },
];

const FeedHeader = ({
  item,
}: {
  item: {
    avatar: string;
    username: string;
    date: string;
  };
}) => {
  return (
    <View
      style={{
        height: sizes(64),
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: sizes(24),
      }}>
      <Image
        source={{
          uri: item.avatar,
        }}
        style={{
          height: sizes(64),
          width: sizes(64),
          borderRadius: sizes(64) / 2,
        }}
      />
      <View style={{padding: sizes(20)}}>
        <Text>{item.username}</Text>
        <Text>{item.date}</Text>
      </View>
    </View>
  );
};

const FeedCaption = ({item}: {item: {caption: string | null}}) => {
  const [showMore, setShowMore] = useState(false);
  const [longText, setLongText] = useState(false);

  const onTextLayout = useCallback((e: {nativeEvent: {lines: any}}) => {
    if (e.nativeEvent.lines.length > 5) {
      setLongText(true);
    }
  }, []);

  if (item?.caption) {
    return (
      <View
        style={{
          marginTop: sizes(24),
          marginHorizontal: sizes(24),
        }}>
        <Text
          onTextLayout={e => onTextLayout(e)}
          numberOfLines={showMore ? undefined : 5}>
          {item.caption}
        </Text>
        {longText ? (
          <Text onPress={() => setShowMore(!showMore)} style={{color: 'blue'}}>
            {showMore ? 'Less' : 'More'}
          </Text>
        ) : (
          <></>
        )}
      </View>
    );
  }
  return null;
};

const FeedImage = ({item}: {item: {image: string | null}}) => {
  if (item?.image) {
    return (
      <View style={{flex: 1, marginTop: sizes(24)}}>
        <Image
          source={{uri: item.image}}
          style={{height: '100%', width: '100%'}}
        />
      </View>
    );
  }
  return null;
};

const Feed = ({
  item,
}: {
  item: {
    avatar: string;
    username: string;
    date: string;
    caption: string | null;
    image: string | null;
    comment: number;
    like: number;
  };
}) => {
  return (
    <View style={{marginTop: sizes(20)}}>
      <FeedHeader item={item} />
      <View style={{height: sizes(483)}}>
        <FeedCaption item={item} />
        <FeedImage item={item} />
      </View>
      <View
        style={{
          height: sizes(52),
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <IconButton
            icon={'camera'}
            iconColor="grey"
            size={sizes(20)}
            onPress={() => console.log('pressed')}
          />
          <Text>{item.comment}</Text>
          <IconButton
            icon={'camera'}
            iconColor="grey"
            size={sizes(20)}
            onPress={() => console.log('pressed')}
          />
        </View>
        <View style={{flexDirection: 'row'}}>
          <IconButton
            icon={'camera'}
            iconColor="grey"
            size={sizes(20)}
            onPress={() => console.log('pressed')}
          />
          <IconButton
            icon={'camera'}
            iconColor="grey"
            size={sizes(20)}
            onPress={() => console.log('pressed')}
          />
          <IconButton
            icon={'camera'}
            iconColor="grey"
            size={sizes(20)}
            onPress={() => console.log('pressed')}
          />
        </View>
      </View>
    </View>
  );
};

const HomeScreen = () => {
  return (
    <View style={{flex: 1}}>
      <FlatList data={data} renderItem={item => <Feed {...item} />} />
      <TouchableOpacity
        style={{
          height: sizes(50),
          width: sizes(50),
          borderRadius: sizes(50) / 2,
          zIndex: 99,
          bottom: 62,
          right: 20,
          backgroundColor: '#00ADB5',
          position: 'absolute',
        }}
        onPress={() => console.log('pressed')}></TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
