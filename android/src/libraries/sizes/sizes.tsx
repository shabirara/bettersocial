import {ms} from 'react-native-size-matters';

function sizes(size: number) {
  return ms(size);
}

export default sizes;
